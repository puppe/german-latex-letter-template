# LaTeX-Vorlage für deutsche Briefe

Sie finden hier eine LaTeX-Vorlage für deutsche Briefe nach DIN 5008; umgesetzt
auf Basis der [KOMA-Script](https://www.komascript.de/)-Klasse scrlttr2.

## Benutzung

Wesentlich sind vor allem die Dateien [absender.lco](./absender.lco) und
[brief.tex](./brief.tex). Legen Sie diese im selben Verzeichnis ab, editieren
Sie sie nach Ihren Wünschen und erzeugen Sie dann das fertige Dokument, so wie
Sie das mit LaTeX gewohnt sind. Das sollte eigentlich mit jeder gängigen
TeX-Distribution funktionieren.

### Makefile

Es liegt auch ein Makefile bei. Sie können also mit dem Befehl `make` ganz
einfach eine PDF-Datei (brief.pdf) erstellen. Mit `make clean` löschen Sie alle
automatisch erzeugten Dateien.

### Nix

Wenn Sie den Paketmanager Nix mit dem Feature
[Flakes](https://nixos.wiki/wiki/Flakes) verwenden, dann können Sie TeX und alle
notwendigen Abhängigkeiten ganz einfach installieren, indem Sie den Befehl `nix
develop` eingeben. Sie erhalten dann eine Shell, in der Sie `make` wie oben
beschrieben aufrufen können. Wenn Sie
[nix-direnv](https://github.com/nix-community/nix-direnv) verwenden, erhalten
Sie eine solche Shell sogar, ohne einen Befehl eigeben zu müssen. Sie müssen
einfach nur in das Verzeichnis dieses Repositorys wechseln.

## Lizenz

Lizenziert unter der EUPL, nur Version 1.2 ("Lizenz"); Sie dürfen dieses Werk
ausschließlich gemäß dieser Lizenz nutzen. Eine Kopie der Lizenz finden Sie
hier:

https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher
Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie
ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder
stillschweigend - verbreitet. Die sprachspezifischen Genehmigungen und
Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.
