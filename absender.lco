% Copyright 2022 Martin Puppe
%
% Lizenziert unter der EUPL, nur Version 1.2 ("Lizenz"); Sie dürfen dieses Werk
% ausschließlich gemäß dieser Lizenz nutzen. Eine Kopie der Lizenz finden Sie
% hier:
%
% https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
%
% Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in
% schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software
% "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich
% oder stillschweigend - verbreitet. Die sprachspezifischen Genehmigungen und
% Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.

\ProvidesFile{absender.lco}

\KOMAoptions{%
% fromemail=true,       % Email wird im Briefkopf angezeigt
% fromphone=true,       % Telefonnumer wird im Briefkopf angezeigt
% fromfax=true,         % Faxnummer wird im Briefkopf angezeigt
% fromurl=true,         % URL wird im Briefkopf angezeigt
% fromlogo=true,        % Logo wird im Briefkopf angezeigt
% subject=titled,       % Druckt "Betrifft: " vor dem Betreff
locfield=wide,          % Breite Absenderergänzung (location)
fromalign=left,         % Ausrichtung des Briefkopfes
fromrule=afteraddress%  % Trennlinie unter dem Briefkopf
}

\RequirePackage[utf8]{inputenc}
\RequirePackage[ngerman]{babel}

\setkomavar{fromname}{Max Mustermann} % Name
\setkomavar{fromaddress}{% % Adresse
  Musterstr. 42\\
  01234 Musterstadt%
}
\setkomavar{fromfax}{01234~56789} % Faxnummer
\setkomavar{fromemail}{max.muster@muster.com} % Email-Adresse
\setkomavar{fromphone}{01234~56789} % Telefonnummer
\setkomavar{fromurl}[Website:~]{www.muster.com} % Website

% ===== Absenderergänzung =====
\setkomavar{location}{%
  \raggedright\footnotesize{%
  \usekomavar{fromname}\\
  \usekomavar{fromaddress}\\
  \usekomavar*{fromphone}\usekomavar{fromphone}\\
  \usekomavar*{fromfax}\usekomavar{fromfax}\\
  \usekomavar*{fromemail}\usekomavar{fromemail}
  \usekomavar*{fromurl}\usekomavar{fromurl}}%
}
% ============================

% Logo
% \setkomavar{fromlogo}{\includegraphics{logo.png}}

% Die Bankverbindung wird nicht automatisch verwendet. Dazu muss bspw. mittels \firstfoot ein eigener Brieffuß definiert werden.
\setkomavar{frombank}{}

% ===== Signatur =====
\setkomavar{signature}{%
  \usekomavar{fromname}\\
  Geschäftsführer%
}
\renewcommand*{\raggedsignature}{\raggedright}
% ====================
