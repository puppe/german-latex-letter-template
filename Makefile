# Copyright 2022 Martin Puppe
#
# Lizenziert unter der EUPL, nur Version 1.2 ("Lizenz"); Sie dürfen dieses Werk
# ausschließlich gemäß dieser Lizenz nutzen. Eine Kopie der Lizenz finden Sie
# hier:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in
# schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software
# "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich
# oder stillschweigend - verbreitet. Die sprachspezifischen Genehmigungen und
# Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.

default: brief.pdf

.PHONY: default clean

brief.pdf: latexmk/brief.pdf
	cp latexmk/brief.pdf brief.pdf

latexmk/brief.pdf: brief.tex absender.lco
	latexmk -lualatex -output-directory=latexmk brief.tex

clean:
	-rm -r latexmk
	-rm brief.pdf
