/*
Copyright 2022 Martin Puppe

Lizenziert unter der EUPL, nur Version 1.2 ("Lizenz"); Sie dürfen dieses Werk
ausschließlich gemäß dieser Lizenz nutzen. Eine Kopie der Lizenz finden Sie
hier:

https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher
Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie
ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder
stillschweigend - verbreitet. Die sprachspezifischen Genehmigungen und
Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.
*/

{
  inputs.flake-utils.url = "github:numtide/flake-utils";
  outputs = { self, nixpkgs, flake-utils }:

    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        tl = pkgs.texlive.combine {
          inherit (pkgs.texlive)
            scheme-small latexmk selnolig;
        };
      in {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs;
            [ tl pandoc ];
        };
      }

    );
}
